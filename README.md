# Paasword PHP Laravel Middleware

[Paasword](https://www.paas-word.com) is an online authentication, authorization and user management service.
This middleware library by [Paasword](https://www.paas-word.com) for Laravel PHP Web Framework enables website owners with a Laravel backend to restrict their endpoints to authenticated users only and retrieve user data. 

## Usage

1. Create a free account at [Paasword](https://www.paas-word.com) website.
2. Recieve a login, sign-up, account and forgot-password pages for your website based on the user attributes you set up.
3. Set the callback pages on your website where users will be redirected after they sign-up and log in. 
4. Once a user is redirected to your website with a token, send this token to your backend in the "x-auth-token" header.

## Installation

`composer require paasword/laravel`

## Set Private Key as Environment Variable
Create an app on [Paasword](https://www.paas-word.com) and then set its Private Key as an environment variable on your Laravel .env file.

`PAASWORD_APP_PRIVATE_KEY="93f56f52-957d-4953-93a6-c5492e79778b"`


## Gaurd all endpoints
Guard all endpoints against unauthorized users

```php
// app/Http/Kernel.php

protected $middleware = [
    ...
    \Paasword\PaaswordMiddleware::class,
];
```

## Gaurd spesific routes against unauthenticated users
You should place these gaurds on routes dedicated to logged-in users. 

```php
// app/Http/Kernel.php

protected $routeMiddleware = [
    ...
    'paasword' => \Paasword\PaaswordMiddleware::class,
];

// routes/api.php

Route::middleware('paasword')->get('private', function (Request $request) {
    return 'Hello from private';
});
```

## Retrieve user information

```php
Route::middleware('paasword')->get('private', function (Request $request) {
    var_dump($request->user);
    return 'Hello from private';
});
```

