<?php

namespace Paasword;

use Closure;
use Exception;
use Illuminate\Http\Request;
use \Firebase\JWT\JWT;

class PaaswordMiddleware
{
	public function handle(Request $request, Closure $next)
	{
        try {
            $token = $request->headers->get('x-auth-token');
            if (!$token) {
                return response("'x-auth-token' header required", 401);
            }

            $appPrivateKey = env('PAASWORD_APP_PRIVATE_KEY');
            if (!$appPrivateKey) {
                return response("An App Private Key Environment Variable is required", 401);
            }

            $user = JWT::decode($token, $appPrivateKey, ['HS256']);

            if ($user && $user->AutoLogout->IsEnabled) {
                $loginTime = $user->iat * 1000;
                $hoursSinceLogin = round((time() - $loginTime)/3600000);
                if ($hoursSinceLogin > $user->AutoLogout->LogoutEveryXHours) {
                    return response("Session expired", 401);
                }
            }

            $request->user = $user;
            return $next($request);
            
        } catch(Exception $e) {
            return response($e->getMessage(), 401);
        }
	}
}